
// Bài 5: Tính ngày trước và sau ngày nhập liệu
document.getElementById("btnTinhNgayTruocB5").onclick = function () {
  var inputNgayB5 = document.getElementById("inputNgayB5").value * 1;
  var inputThangB5 = document.getElementById("inputThangB5").value * 1;
  var inputNamB5 = document.getElementById("inputNamB5").value * 1;
  var namNhuan = null;
  var namThuong = null;
  var maxFebDate = null;
  var ngayHomQua = inputNgayB5 - 1;
  // var ngayMai = inputNgayB5 + 1;
  var thangTruoc = inputThangB5 - 1;
  // var thangSau = inputThangB5 + 1;
  var namTruoc = inputNamB5 - 1;
  // var namSau = inputNamB5 + 1;
  var showNgayB5 = document.getElementById("showNgayB5");
  // Check data input for year & month
  if (inputNgayB5 > 31 || inputThangB5 > 12) {
    alert("Thế giới này không có ngày và tháng như vậy :)");
  }
  // Check leap year (năm nhuận)
  if ((inputNamB5 % 4 == 0 && inputNamB5 % 100 != 0) || inputNamB5 % 400 == 0) {
    namNhuan = inputNamB5;
  } else {
    namThuong = inputNamB5;
  }
  // Check Febbruary in leap year & normal year
  if (inputNamB5 == namNhuan) {
    maxFebDate = 29;
  } else {
    maxFebDate = 28;
  }
  // Check input if leap year and month = Feb, alert
  if (inputNamB5 == namNhuan && inputThangB5 == 2 && inputNgayB5 > maxFebDate) {
    alert(
      "Bạn đang chọn năm Nhuận và tháng 2 chỉ có tối đa 29 ngày, vui lòng nhập lại."
    );
  } else if (
    inputNamB5 == namThuong &&
    inputThangB5 == 2 &&
    inputNgayB5 > maxFebDate
  ) {
    alert("Tháng 2 chỉ có tối đa 28 ngày, vui lòng nhập lại.");
  }
  // All cases for previous date
  if (
    (inputNgayB5 == 1 && inputThangB5 == 5) ||
    (inputNgayB5 == 1 && inputThangB5 == 7) ||
    (inputNgayB5 == 1 && inputThangB5 == 10) ||
    (inputNgayB5 == 1 && inputThangB5 == 12)
  ) {
    ngayHomQua = 30;
    inputThangB5 = --inputThangB5;
    showNgayB5.innerHTML = ngayHomQua + "/" + inputThangB5 + "/" + inputNamB5;
  } else if (
    (inputNgayB5 == 1 && inputThangB5 == 2) ||
    (inputNgayB5 == 1 && inputThangB5 == 4) ||
    (inputNgayB5 == 1 && inputThangB5 == 6) ||
    (inputNgayB5 == 1 && inputThangB5 == 8) ||
    (inputNgayB5 == 1 && inputThangB5 == 9) ||
    (inputNgayB5 == 1 && inputThangB5 == 11)
  ) {
    ngayHomQua = 31;
    thangTruoc = --inputThangB5;
    showNgayB5.innerHTML = ngayHomQua + "/" + thangTruoc + "/" + inputNamB5;
  } else if (inputNgayB5 == 1 && inputThangB5 == 3 && inputNamB5 == namNhuan) {
    ngayHomQua = 29;
    thangTruoc = --inputThangB5;
    showNgayB5.innerHTML = ngayHomQua + "/" + thangTruoc + "/" + inputNamB5;
  } else if (inputNgayB5 == 1 && inputThangB5 == 3 && inputNamB5 == namThuong) {
    ngayHomQua = 28;
    thangTruoc = --inputThangB5;
    showNgayB5.innerHTML = ngayHomQua + "/" + thangTruoc + "/" + inputNamB5;
  } else if (inputNgayB5 == 1 && inputThangB5 == 1) {
    ngayHomQua = 31;
    thangTruoc = 12;
    namTruoc = --inputNamB5;
    showNgayB5.innerHTML = ngayHomQua + "/" + thangTruoc + "/" + namTruoc;
  } else {
    showNgayB5.innerHTML = ngayHomQua + "/" + inputThangB5 + "/" + inputNamB5;
  }
};
document.getElementById("btnTinhNgaySauB5").onclick = function () {
  var inputNgayB5 = document.getElementById("inputNgayB5").value * 1;
  var inputThangB5 = document.getElementById("inputThangB5").value * 1;
  var inputNamB5 = document.getElementById("inputNamB5").value * 1;
  var namNhuan = null;
  var namThuong = null;
  var maxFebDate = null;
  // var ngayHomQua = inputNgayB5 - 1;
  var ngayMai = inputNgayB5 + 1;
  // var thangTruoc = inputThangB5 -1;
  var thangSau = inputThangB5 + 1;
  // var namTruoc = inputNamB5 - 1;
  var namSau = inputNamB5 + 1;
  var showNgayB5 = document.getElementById("showNgayB5");
  // Check data input for year & month
  if (inputNgayB5 > 31 || inputThangB5 > 12) {
    alert("Thế giới này không có ngày và tháng như vậy :)");
  }
  // Check leap year (năm nhuận)
  if ((inputNamB5 % 4 == 0 && inputNamB5 % 100 != 0) || inputNamB5 % 400 == 0) {
    namNhuan = inputNamB5;
  } else {
    namThuong = inputNamB5;
  }
  // Check Febbruary in leap year & normal year
  if (inputNamB5 == namNhuan) {
    maxFebDate = 29;
  } else {
    maxFebDate = 28;
  }
  // Check input if leap year and month = Feb, alert
  if (inputNamB5 == namNhuan && inputThangB5 == 2 && inputNgayB5 > maxFebDate) {
    alert(
      "Bạn đang chọn năm Nhuận và tháng 2 chỉ có tối đa 29 ngày, vui lòng nhập lại."
    );
  } else if (
    inputNamB5 == namThuong &&
    inputThangB5 == 2 &&
    inputNgayB5 > maxFebDate
  ) {
    alert("Tháng 2 chỉ có tối đa 28 ngày, vui lòng nhập lại.");
  }
  if (
    (inputNgayB5 == 31 && inputThangB5 == 1) ||
    (inputNgayB5 == 31 && inputThangB5 == 3) ||
    (inputNgayB5 == 31 && inputThangB5 == 5) ||
    (inputNgayB5 == 31 && inputThangB5 == 7) ||
    (inputNgayB5 == 31 && inputThangB5 == 8) ||
    (inputNgayB5 == 31 && inputThangB5 == 10)
  ) {
    ngayMai = 1;
    thangSau = ++inputThangB5;
    showNgayB5.innerHTML = ngayMai + "/" + thangSau + "/" + inputNamB5;
  } else if (
    (inputNgayB5 == 30 && inputThangB5 == 4) ||
    (inputNgayB5 == 30 && inputThangB5 == 6) ||
    (inputNgayB5 == 30 && inputThangB5 == 9) ||
    (inputNgayB5 == 30 && inputThangB5 == 11)
  ) {
    ngayMai = 1;
    thangSau = ++inputThangB5;
    showNgayB5.innerHTML = ngayMai + "/" + thangSau + "/" + inputNamB5;
  } else if (inputNgayB5 == 29 && inputThangB5 == 2 && inputNamB5 == namNhuan) {
    ngayMai = 1;
    thangSau = ++inputThangB5;
    showNgayB5.innerHTML = ngayMai + "/" + thangSau + "/" + inputNamB5;
  } else if (
    inputNgayB5 == 28 &&
    inputThangB5 == 2 &&
    inputNamB5 == namThuong
  ) {
    ngayMai = 1;
    thangSau = ++inputThangB5;
    showNgayB5.innerHTML = ngayMai + "/" + thangSau + "/" + inputNamB5;
  } else if (inputNgayB5 == 31 && inputThangB5 == 12) {
    ngayMai = 1;
    thangSau = 1;
    namSau = ++inputNamB5;
    showNgayB5.innerHTML = ngayMai + "/" + thangSau + "/" + namSau;
  } else {
    showNgayB5.innerHTML = ngayMai + "/" + inputThangB5 + "/" + inputNamB5;
  }
};

// Bài 6: Tính số ngày dựa theo tháng & năm nhập liệu
document.getElementById("btnTinhNgayB6").onclick = function () {
  var inputThangB6 = document.getElementById("inputThangB6").value * 1;
  var inputNamB6 = document.getElementById("inputNamB6").value * 1;
  var showNgayB6 = document.getElementById("showNgayB6");
  var namNhuan = null;
  var namThuong = null;
  var maxFebDate = null;
  // Check data input
  if (inputThangB6 > 12) {
    alert("Nhập tháng gì kỳ vậy má");
  }
  if (inputNamB6 == namNhuan) {
    maxFebDate = 29;
  } else {
    maxFebDate = 28;
  }
  // Check leap year (năm nhuận)
  if ((inputNamB6 % 4 == 0 && inputNamB6 % 100 != 0) || inputNamB6 % 400 == 0) {
    namNhuan = inputNamB6;
  } else {
    namThuong = inputNamB6;
  }
  // Check Febbruary in leap year & normal year
  if (inputNamB6 == namNhuan) {
    maxFebDate = 29;
  } else {
    maxFebDate = 28;
  }
  if (
    inputThangB6 == 1 ||
    inputThangB6 == 3 ||
    inputThangB6 == 5 ||
    inputThangB6 == 7 ||
    inputThangB6 == 8 ||
    inputThangB6 == 10 ||
    inputThangB6 == 12
  ) {
    showNgayB6.innerHTML =
      "Tháng " + inputThangB6 + " năm " + inputNamB6 + " có 31 ngày.";
  } else if (
    inputThangB6 == 4 ||
    inputThangB6 == 6 ||
    inputThangB6 == 9 ||
    inputThangB6 == 11
  ) {
    showNgayB6.innerHTML =
      "Tháng " + inputThangB6 + " năm " + inputNamB6 + " có 30 ngày.";
  } else if (inputNamB6 == namNhuan && inputThangB6 == 2) {
    showNgayB6.innerHTML =
      "Tháng " +
      inputThangB6 +
      " năm " +
      inputNamB6 +
      " có " +
      maxFebDate +
      " ngày.";
  } else {
    showNgayB6.innerHTML =
      "Tháng " +
      inputThangB6 +
      " năm " +
      inputNamB6 +
      " có " +
      maxFebDate +
      " ngày.";
  }
};

// Bài 7: Đọc số theo số nhập liệu
document.getElementById("btnDocSoB7").onclick = function () {
  var inputNumberB7 = document.getElementById("inputNumberB7").value * 1;
  var inputHundredB7 = Math.floor(inputNumberB7 / 100);
  var readHundredB7 = "";
  var inputTenthB7 = Math.floor((inputNumberB7 % 100) / 10);
  var readTenthB7 = "";
  var inputUnitB7 = inputNumberB7 % 10;
  var readUnitB7 = "";
  if (inputNumberB7 <= 99 || inputNumberB7 >= 1000) {
    alert("Nhập 3 số mà má ơi");
  }
  switch (inputHundredB7) {
    case 1:
      readHundredB7 = "Một trăm ";
      break;
    case 2:
      readHundredB7 = "Hai trăm ";
      break;
    case 3:
      readHundredB7 = "Ba trăm ";
      break;
    case 4:
      readHundredB7 = "Bốn trăm ";
      break;
    case 5:
      readHundredB7 = "Năm trăm ";
      break;
    case 6:
      readHundredB7 = "Sáu trăm ";
      break;
    case 7:
      readHundredB7 = "Bảy trăm ";
      break;
    case 8:
      readHundredB7 = "Tám trăm ";
      break;
    case 9:
      readHundredB7 = "Chín trăm ";
      break;
    default:
  }
  switch (inputTenthB7) {
    case 0:
      readTenthB7 = "linh ";
      break;
    case 1:
      readTenthB7 = "mười ";
      break;
    case 2:
      readTenthB7 = "hai mươi ";
      break;
    case 3:
      readTenthB7 = "ba mươi ";
      break;
    case 4:
      readTenthB7 = "bốn mươi ";
      break;
    case 5:
      readTenthB7 = "năm mươi ";
      break;
    case 6:
      readTenthB7 = "sáu mươi ";
      break;
    case 7:
      readTenthB7 = "bảy mươi ";
      break;
    case 8:
      readTenthB7 = "tám mươi ";
      break;
    case 9:
      readTenthB7 = "chín mươi ";
      break;
    default:
  }
  switch (inputUnitB7) {
    case 1:
      readUnitB7 = "mốt.";
      break;
    case 2:
      readUnitB7 = "hai.";
      break;
    case 3:
      readUnitB7 = "ba.";
      break;
    case 4:
      readUnitB7 = "bốn.";
      break;
    case 5:
      readUnitB7 = "lăm.";
      break;
    case 6:
      readUnitB7 = "sáu.";
      break;
    case 7:
      readUnitB7 = "bảy.";
      break;
    case 8:
      readUnitB7 = "tám.";
      break;
    case 9:
      readUnitB7 = "chín.";
      break;
    default:
  }
  // Condition to change the read name
  if (
    (inputTenthB7 == 0 && inputUnitB7 == 1) ||
    (inputTenthB7 == 1 && inputUnitB7 == 1)
  ) {
    readUnitB7 = "một.";
  }
  if (
    (inputTenthB7 == 0 && inputUnitB7 == 5)
  ) {
    readUnitB7 = "năm.";
  }
  if (inputTenthB7 == 0 && inputUnitB7 == 0) {
    readTenthB7 = "";
    readUnitB7 = "";
  }
  document.getElementById("showDocSoB7").innerHTML =
    readHundredB7 + readTenthB7 + readUnitB7;
};

// Bài 8: Tìm sinh viên xa trường nhất
document.getElementById("btnTimSinhVienXaNhatB8").onclick = function () {
  var tenSV01B8 = document.getElementById("tenSV01B8").value;
  var tenSV02B8 = document.getElementById("tenSV02B8").value;
  var tenSV03B8 = document.getElementById("tenSV03B8").value;
  var toaDoX01B8 = document.getElementById("toaDoX01B8").value * 1;
  var toaDoX02B8 = document.getElementById("toaDoX02B8").value * 1;
  var toaDoX03B8 = document.getElementById("toaDoX03B8").value * 1;
  var toaDoY01B8 = document.getElementById("toaDoY01B8").value * 1;
  var toaDoY02B8 = document.getElementById("toaDoY02B8").value * 1;
  var toaDoY03B8 = document.getElementById("toaDoY03B8").value * 1;
  var toaDoXTruongB8 = document.getElementById("toaDoXTruongB8").value * 1;
  var toaDoYTruongB8 = document.getElementById("toaDoYTruongB8").value * 1;
  var showTimSinhVienXaNhatB8 = document.getElementById(
    "showTimSinhVienXaNhatB8"
  );
  // Calculate distance
  var dTruongSV01B8 = 0;
  dTruongSV01B8 = Math.sqrt(
    Math.pow(toaDoXTruongB8 - toaDoX01B8, 2) +
      Math.pow(toaDoYTruongB8 - toaDoY01B8, 2)
  );
  var dTruongSV02B8 = 0;
  dTruongSV02B8 = Math.sqrt(
    Math.pow(toaDoXTruongB8 - toaDoX02B8, 2) +
      Math.pow(toaDoYTruongB8 - toaDoY02B8, 2)
  );
  var dTruongSV03B8 = 0;
  dTruongSV03B8 = Math.sqrt(
    Math.pow(toaDoXTruongB8 - toaDoX03B8, 2) +
      Math.pow(toaDoYTruongB8 - toaDoY03B8, 2)
  );
  // Check distance before post result
  console.log("Distance SV1: ", dTruongSV01B8);
  console.log("Distance SV2: ", dTruongSV02B8);
  console.log("Distance SV3: ", dTruongSV03B8);
  if (dTruongSV01B8 > dTruongSV02B8 && dTruongSV01B8 > dTruongSV03B8) {
    showTimSinhVienXaNhatB8.innerHTML =
      "Sinh viên xa trường nhất: " + tenSV01B8;
  } else if (dTruongSV02B8 > dTruongSV01B8 && dTruongSV02B8 > dTruongSV03B8) {
    showTimSinhVienXaNhatB8.innerHTML =
      "Sinh viên xa trường nhất: " + tenSV02B8;
  } else {
    showTimSinhVienXaNhatB8.innerHTML =
      "Sinh viên xa trường nhất: " + tenSV03B8;
  }
};
